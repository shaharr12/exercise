<?php
class Htmlpage{
    protected $title = "";
    protected $body  = "";

    public function view(){
        echo  "<html>
        <head>
          <title>$this->title</title>
        </head>
        <body> $this->body</body>
        </html>";
    }
    function __construct($title = "",$body = ""){
        if($title != ""){
            $this->title = $title;
        }
            if($body != ""){
            $this->body = $body;

        }
        
    }
    }

    class coloredText extends Htmlpage {
        protected $color = "black";
        public function __set($property,$value){
            if($property == 'color'){
                $colors = array('red','yellow','green','blue','brown');
            
            if(in_array($value,$colors)){
                $this->color = $value;  
         }
         else{
             $this->title = "";
             $this->body = 'color not exsist';
         }
        }
       }
       public function view(){
           
        echo  "<html>
        <head>
          <title style = 'color:$this->color'>$this->title </title>
        </head>
        <body style = 'color:$this->color'>$this->body</body>
        </html>";
        }
        

        }
    
        class FontText extends coloredText{
            protected $font = '26';
            public function __set($property,$value){
                parent::__set($property,$value);
                if($property == 'font'){
                    $fonts = range(10,24);
                
                if(in_array($value,$fonts)){
                    $this ->font = $value;
                }
                else{
                    exit('!!No valid font size , please insert again');
                }
            }
        }
    
        public function view(){
           
            echo  "<html>
            <head>
              <title style = 'color:$this->color'>$this->title </title>
            </head>
            <body>
            <p style = 'color:$this->color;font-size:$this->font'>$this->body</p>
            </body>
            </html>";
            }
            }
        
?>